import './styles/main.css'
import './styles/main.less'

import {JSComponent} from './components/js-component/js-component'
import D3Component from './components/d3-component/d3-component'

var jsComponent = new JSComponent()
var d3Component = new D3Component()
