import './d3-component.less'
import template from './d3-component.html'
import d3 from 'd3'

class D3Component {
	constructor() {
		d3.select('body')
			.append(() => document.createElement('div'))
			.classed('d3-component', true)			
			.html(template)
	}
}

export default D3Component