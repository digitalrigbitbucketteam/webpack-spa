var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin')
var WebpackOnBuildPlugin = require('on-build-webpack');
var webpackConfig = require('./webpack.config.js');
webpackConfig.plugins.shift()

module.exports = function(config) {
  config.set({
    basePath: '',

    frameworks: ['mocha'],
    reporters: ['mocha','coverage'],
    // reporter options
    mochaReporter: {
      output: 'autowatch'
    },
    files: [
      'test/test.js'
    ],

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 8080,

    preprocessors: {
      // add webpack as preprocessor
      'test/test.js': ['webpack', 'sourcemap'],
      'src/*.js': ['webpack']
    },

    webpack: {
      // webpack configuration
      devtool: '#inline-source-map',
      module: {
        loaders: webpackConfig.module.loaders
      },
      plugins: webpackConfig.plugins,
      resolve:{
        fallback:[
          path.join(__dirname, 'vendor'),
        ],
        alias: webpackConfig.resolve.alias
      },
      noParse: [
        /vendor/
      ]
    },
    webpackMiddleware: {
     // webpack-dev-middleware configuration
     // i. e.
     noInfo: true
    },
    plugins: [
      'karma-webpack',
      'karma-mocha',
      'karma-sourcemap-loader',
      'karma-chrome-launcher',
      'karma-mocha-reporter',
      'karma-istanbul-reporter',
      'karma-coverage',
      'karma-teamcity-reporter'
    ],

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_ERROR,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['Chrome'],

    singleRun: false,

    // optionally, configure the reporter
    coverageReporter: {
      type : 'html',
      dir : 'coverage/'
    }

  });
};
